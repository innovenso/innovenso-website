<div class="container-fluid banner">
    <div class="container">
        <section class="row">
            <div class="col-12 text-center">
                <h1>Our Strategy</h1>
                <p>We are proud that we can offer you top-notch architecture guidance and innovative software development. With us, you will never feel that architects and designers belong in an ivory tower.</p>
            </div>
        </section>

        <section class="row card-deck mb-3 text-center strategies">
            <div class="col-lg-4 mb-4 strategy">
                <div class="card">
                    <div class="card-body">
                        <h1>1</h1>
                        <h2>Innovate</h2>
                        <p>We constantly keep up with new technology evolutions and know when and where to apply them.</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4 strategy">
                <div class="card">
                    <div class="card-body">
                        <h1>2</h1>
                        <h2>Simplify</h2>
                        <p>We bring clarity to complexity and have a knack for finding smart ways to simplify.</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4 strategy">
                <div class="card">
                    <div class="card-body">
                        <h1>3</h1>
                        <h2>Harmonise</h2>
                        <p>We look beyond designs and help you set up the perfect organisation to deliver your vision and achieve your objectives.</p>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>