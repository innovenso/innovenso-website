<% include "head.gsp" %>
<% include "menu.gsp" %>

<div class="container-fluid content">
    <div class="container">
        <section class="row">
            <div class="col-lg-4">
                <h1>Contact us!</h1>
            </div>

            <div class="col-lg-8 mt-2">
                <h2>Virginie Héloire</h2>
                <p>virginie@innovenso.com</p>
                <p>+33 6 69 67 71 98</p>
                <h2>Jurgen Lust</h2>
                <p>jurgen@innovenso.com</p>
                <p>+32 497 50 89 11</p>
            </div>
        </section>
    </div>
</div>

<% include "footer.gsp" %>
<% include "tail.gsp" %>