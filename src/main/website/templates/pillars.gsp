<div class="container pillars">
    <div class="row card-deck mb-3 text-center">
        <div class="col-lg-4 mb-4 pillar">
            <div class="card">
                <div class="card-body">
                    <i class="icon far fa-hands-helping"></i>

                    <h1>Your IT Leadership Partner</h1>

                    <p>We help companies set up a mature architecture function and refine their IT strategy and delivery organisation.</p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 mb-4 pillar">
            <div class="card">
                <div class="card-body">
                    <i class="icon far fa-compass"></i>

                    <h1>Your Technology Advisor</h1>

                    <p>We support IT teams with technology choices and roadmaps, as part of a cloud, data or digital strategy.</p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 mb-4 pillar">
            <div class="card">
                <div class="card-body">
                    <i class="icon far fa-laptop-code"></i>

                    <h1>Your Software Craftsman</h1>

                    <p>We deliver innovative software solutions and coach development teams in agile delivery, software craftsmanship and clean architecture.</p>
                </div>
            </div>
        </div>
    </div>
</div>