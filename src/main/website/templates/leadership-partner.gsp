<div class="container-fluid content">
    <div class="container">
        <section class="row">
            <div class="col-lg-4">
                <h1>Your IT Leadership <span class="emphasis">Partner</span></h1>
            </div>

            <div class="col-lg-8 mt-2">
                <p>We help companies set up a mature architecture function and refine their IT strategy and delivery organisation.</p>

                <ul>
                    <li>Get started with an architecture function</li>
                    <li>Enterprise and Solution Architecture framework and ways of working</li>
                    <li>Business and IT Target operating model</li>
                    <li>IT organisation change management</li>
                    <li>IT vision and road map</li>
                    <li>IT strategy communication plan and tools</li>
                    <li>Target architecture and transition states</li>
                    <li>Blueprint or high level design to support an IT transformation programme</li>
                </ul>
            </div>
        </section>
    </div>
</div>