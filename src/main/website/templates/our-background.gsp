<div class="container-fluid content">
    <div class="container">
        <section class="row">
            <div class="col-lg-4">
                <h1>Make a difference, look for solutions, <span class="emphasis">do it right.</span></h1>
            </div>

            <div class="col-lg-8 mt-2">
                <p>We have a proven track record of helping organizations deliver value at speed, while avoiding high
                entropy. We strongly believe in servant leadership, lean and evolutionary architecture. We help companies set up and evolve
                the architecture practice through their digital transformation journey.</p>

                <p>Both of us have almost 2 decades of experience in software development and architecture. We have worked on
                numerous projects of all sizes, delivering
                business value while keeping things as simple and efficient as possible. We generally prefer a no-nonsense, pragmatic
                approach over high principles.</p>
            </div>
        </section>
    </div>
</div>