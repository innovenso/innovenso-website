<div class="container values-home">
    <div class="row">
        <div class="col-lg-4">
            <h2>Why work with Innovensō?</h2>
            <h3>our values</h3>
        </div>
        <div class="col-lg-8 mt-2">
            <p><span class="value">Make a real difference.</span>
                Our talents and relentless drive to bring value to our customers and help them achieve their objectives.
                We don’t only advise, we make it happen!</p>
            <p><span class="value">Constantly look for solutions.</span>
                We strongly believe in continuous improvement. Our test-and-learn approach and evolutionary
                architecture mindset help us focus on solving the right problems.</p>
            <p><span class="value">Do it right or don’t do it!</span>
                Our combined experience gives us the confidence to have a unique offering. Over the last 20 years, we
                have built expertise and a network of professionals across industries and markets, that we want to share
                with our customers.</p>
        </div>
    </div>
</div>