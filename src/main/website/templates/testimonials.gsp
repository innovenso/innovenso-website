<div class="container-fluid testimonials-home">
    <div class="container">
        <div class="row">
            <section class="col-12 text-center">
                <h1>Satisfied Client Stories</h1>

                <div class="bd-example">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <% testimonials.eachWithIndex { it, i -> %>
                            <li data-target="#carouselExampleCaptions" data-slide-to="${i}"
                                class="${i == 0 ? 'active' : ''}"></li>
                            <% } %>
                        </ol>

                        <div class="carousel-inner">

                            <% testimonials.eachWithIndex { it, i -> %>

                            <div class="carousel-item ${i == 0 ? 'active' : ''}">
                                <div class="testimonial">
                                    <img src="${content.rootpath ?: ''}images/testimonials/${it.image}"
                                         class="rounded-circle">
                                    ${it.body}
                                </div>

                                <div class="carousel-caption d-none d-md-block">
                                    <h5>${it.author}</h5>

                                    <p>${it.title}</p>
                                </div>
                            </div>

                            <% } %>

                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>