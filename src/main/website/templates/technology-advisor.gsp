<div class="container-fluid content">
    <div class="container">
        <section class="row">
            <div class="col-lg-4">
                <h1>Your Technology <span class="emphasis">Advisor</span></h1>
            </div>

            <div class="col-lg-8 mt-2">
                <p>We support IT teams with technology choices and roadmaps, as part of a cloud, data or digital strategy.</p>

                <ul>
                    <li>Refine technology directions : standards, principles and patterns</li>
                    <li>Technology or solution choices based on objective criteria</li>
                    <li>Supporting major architecture changes (e.g.: omnichannel, real-time, API, personalisation, ...)</li>
                    <li>Defining a cloud strategy and migration path</li>
                    <li>Support company growth and competitiveness through Digital Transformation</li>
                    <li>Increase data intelligence through AI and ML</li>
                    <li>Architecture Governance and Guidance to development/delivery teams</li>
                    <li>Domain Driven Design</li>
                </ul>
            </div>
        </section>
    </div>
</div>