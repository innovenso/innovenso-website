<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display">
    <link rel="stylesheet" href="${content.rootpath ?: ''}css/innovenso-min.css"/>

    <title>Innovenso - ${content.title ?: ''}</title>

    <meta name="description" content="We help organisations to build their architecture practice supporting innovations and digital transformations.">
    <meta name="author" content="Jurgen Lust">
    <meta name="keywords" content="architecture,digital transformation,technology,cloud,enterprise architecture,solution architecture,CTO,CIO,roadmap,vision">
</head>
<body>

