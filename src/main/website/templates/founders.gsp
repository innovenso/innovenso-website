<div class="container-fluid content2">
    <div class="container">
        <section class="row">
            <div class="col-12 text-center">
                <h1>Meet the Founders</h1>
                <p>We are focused IT leaders with a wealth of experience in business and IT architecture and software development across the world.</p>
            </div>
        </section>

        <section class="row card-deck mb-3 text-center founders">
            <div class="col-lg-6 mb-10 founder">
                <div class="card">
                    <img src="${content.rootpath ?: ''}images/jurgen2.png" class="card-img-top" alt="Jurgen">
                    <h5 class="card-title">Jurgen Lust</h5>
                    <h6 class="card-subtitle mb-2 text-muted">IT Leadership Partner, Architect, Master Software Craftsman</h6>
                    <p><a href="https://download.innovenso.io/jurgen_lust-latest.pdf">Download Jurgen's CV here.</a></p>
                </div>
            </div>

            <div class="col-lg-6 founder">
                <div class="card">
                    <img src="${content.rootpath ?: ''}images/virginie2.png" class="card-img-top" alt="Virginie">
                    <h5 class="card-title">Virginie Héloire</h5>
                    <h6 class="card-subtitle mb-2 text-muted">IT Leadership Partner, Head of Architecture, Enterprise Architect</h6>
                    <p><a href="https://download.innovenso.io/virginie_heloire-latest.pdf">Download Virginie's CV here.</a></p>
                </div>
            </div>
        </section>

    </div>
</div>