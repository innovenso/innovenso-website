<% include "head.gsp" %>
<% include "menu.gsp" %>

<% include "our-background.gsp" %>
<% include "strategy.gsp" %>
<% include "founders.gsp" %>

<% include "footer.gsp" %>
<% include "tail.gsp" %>