<div class="container-fluid content">
    <div class="container">
        <section class="row">
            <div class="col-lg-4">
                <h1>Supporting families with <span class="emphasis">special needs children</span></h1>
            </div>

            <div class="col-lg-8 mt-2">
                <p>A significant part of our revenue is allocated to fund the development of a personal assistance platform and tools
                for families with special needs children.</p>

                <p>We facilitate collaboration and access to personalized activities and tools to the family and everyone supporting the child.</p>

                
            </div>
        </section>
    </div>
</div>