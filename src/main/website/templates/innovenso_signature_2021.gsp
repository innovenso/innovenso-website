<table cellpadding="0" cellspacing="0"
       style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td><table
        cellpadding="0" cellspacing="0"
        style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td
            style="vertical-align: top;"><table cellpadding="0" cellspacing="0"
                                                style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td
                    style="text-align: center;"><img
                        src="https://innovenso.com/images/${content.avatar}" role="presentation"
                        width="130" style="max-width: 128px; display: block;"></td></tr><tr><td
                    height="30"></td></tr><tr><td style="text-align: center;"><img
                    src="https://innovenso.com/images/email-logo.png" role="presentation" width="130"
                    style="max-width: 130px; display: block;"></td></tr><tr><td
                    height="30"></td></tr><tr><td style="text-align: center;"><table cellpadding="0" cellspacing="0"

                                                                                     style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial; display: inline-block;"><tbody><tr
                        style="text-align: center;">
                    <% if (content.twitter) {%>
                    <td><a href="${content.twitter}" color="#08888c"

                                                           style="display: inline-block; padding: 0px; background-color: rgb(8, 136, 140);"><img
                                src="https://innovenso.com/images/twitter-icon-2x.png"
                                alt="twitter" color="#08888c" height="24"
                                style="background-color: rgb(8, 136, 140); max-width: 135px; display: block;"></a>
                    </td><td width="5"><div></div></td>
                    <%}%>

                    <% if (content.linkedin) {%>
                    <td><a href="${content.linkedin}"
                                                              color="#08888c"
                                                              style="display: inline-block; padding: 0px; background-color: rgb(8, 136, 140);"><img
                                src="https://innovenso.com/images/linkedin-icon-2x.png"
                                alt="linkedin" color="#08888c" height="24"
                                style="background-color: rgb(8, 136, 140); max-width: 135px; display: block;"></a>
                    </td>

                    <td width="5"><div></div></td>

                    <%}%>

                    <% if (content.instagram) {%>
                        <td><a href="${content.instagram}"
                                                              color="#08888c"
                                                              style="display: inline-block; padding: 0px; background-color: rgb(8, 136, 140);"><img
                                src="https://innovenso.com/images/instagram-icon-2x.png"
                                alt="instagram" color="#08888c" height="24"
                                style="background-color: rgb(8, 136, 140); max-width: 135px; display: block;"></a>
                    </td><td width="5"><div></div></td><%}%>

                </tr></tbody></table></td></tr></tbody></table></td><td
            width="46"><div></div></td><td style="padding: 0px; vertical-align: middle;"><h3 color="#251a06"
                                                                                             style="margin: 0px; font-size: 18px; color: rgb(37, 26, 6);"><span>${content.author}</span>
    </h3>

        <p color="#251a06" font-size="medium"
           style="margin: 0px; color: rgb(37, 26, 6); font-size: 14px; line-height: 22px;"><span>${content.title}</span>
        </p>

        <p color="#251a06" font-size="medium"
           style="margin: 0px; font-weight: 500; color: rgb(37, 26, 6); font-size: 14px; line-height: 22px;"><span>Innovensō</span>
        </p><table cellpadding="0" cellspacing="0"
                   style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial; width: 100%;"><tbody><tr><td
                    height="30"></td></tr><tr><td color="#8c834f" direction="horizontal" height="1"
                                                  style="width: 100%; border-bottom: 1px solid rgb(140, 131, 79); border-left: none; display: block;"></td>
            </tr><tr><td height="30"></td></tr></tbody></table><table cellpadding="0" cellspacing="0"

                                                                      style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr
                    height="25" style="vertical-align: middle;"><td width="30" style="vertical-align: middle;"><table
                        cellpadding="0" cellspacing="0"
                        style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td
                            style="vertical-align: bottom;"><span color="#8c834f" width="11"
                                                                  style="display: block; background-color: rgb(140, 131, 79);"><img
                                    src="https://innovenso.com/images/phone-icon-2x.png"
                                    color="#8c834f" width="13"
                                    style="display: block; background-color: rgb(140, 131, 79);"></span></td></tr>
                    </tbody></table></td><td style="padding: 0px; color: rgb(37, 26, 6);"><a
                        href="tel:${content.telephone}"
                        color="#251a06"
                        style="text-decoration: none; color: rgb(37, 26, 6); font-size: 12px;"><span>${content.telephone}</span>
                </a></td></tr><tr height="25" style="vertical-align: middle;"><td width="30"
                                                                                  style="vertical-align: middle;"><table
                        cellpadding="0" cellspacing="0"
                        style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td
                            style="vertical-align: bottom;"><span color="#8c834f" width="11"
                                                                  style="display: block; background-color: rgb(140, 131, 79);"><img
                                    src="https://innovenso.com/images/email-icon-2x.png"
                                    color="#8c834f" width="13"
                                    style="display: block; background-color: rgb(140, 131, 79);"></span></td></tr>
                    </tbody></table></td><td style="padding: 0px;"><a href="mailto:${content.email}" color="#251a06"
                                                                      style="text-decoration: none; color: rgb(37, 26, 6); font-size: 12px;"><span>${content.email}</span>
            </a></td></tr><tr height="25" style="vertical-align: middle;"><td width="30"
                                                                              style="vertical-align: middle;"><table
                        cellpadding="0" cellspacing="0"
                        style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td
                            style="vertical-align: bottom;"><span color="#8c834f" width="11"
                                                                  style="display: block; background-color: rgb(140, 131, 79);"><img
                                    src="https://innovenso.com/images/link-icon-2x.png"
                                    color="#8c834f" width="13"
                                    style="display: block; background-color: rgb(140, 131, 79);"></span></td></tr>
                    </tbody></table></td><td style="padding: 0px;"><a href="//www.innovenso.com" color="#251a06"
                                                                      style="text-decoration: none; color: rgb(37, 26, 6); font-size: 12px;"><span>www.innovenso.com</span>
            </a></td></tr><tr height="25" style="vertical-align: middle;"><td width="30"
                                                                              style="vertical-align: middle;"><table
                        cellpadding="0" cellspacing="0"
                        style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td
                            style="vertical-align: bottom;"><span color="#8c834f" width="11"
                                                                  style="display: block; background-color: rgb(140, 131, 79);"><img
                                    src="https://innovenso.com/images/address-icon-2x.png"
                                    color="#8c834f" width="13"
                                    style="display: block; background-color: rgb(140, 131, 79);"></span></td></tr>
                    </tbody></table></td><td style="padding: 0px;"><span color="#251a06"
                                                                         style="font-size: 12px; color: rgb(37, 26, 6);"><span>${content.street}, ${content.postal} ${content.city}, Belgium</span>
            </span></td></tr></tbody></table><table cellpadding="0" cellspacing="0"
                                                    style="vertical-align: -webkit-baseline-middle; font-size: medium; font-family: Arial;"><tbody><tr><td
                    height="30"></td></tr></tbody></table><a href="https://innovenso.com"
                                                             target="_blank" rel="noopener noreferrer"
                                                             style="font-size: 12px; display: block; color: rgb(37, 26, 6);">We do it right, or we don't do it.</a></td>
    </tr></tbody></table></td></tr></tbody></table>






