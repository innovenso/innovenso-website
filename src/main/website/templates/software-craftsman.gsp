<div class="container-fluid content">
    <div class="container">
        <section class="row">
            <div class="col-lg-4">
                <h1>Your Software <span class="emphasis">Craftsman</span></h1>
            </div>

            <div class="col-lg-8 mt-2">
                <p>We deliver innovative software solutions and coach development teams in agile delivery, software craftsmanship and clean architecture.</p>

                <ul>
                    <li>Cloud native solutions on the major public cloud platforms</li>
                    <li>Introducing agile and lean principles in software development</li>
                    <li>Guiding software refactoring and migration</li>
                    <li>Guidance in microservice architectures and service choreography</li>
                    <li>Chaos engineering - building scalable, resilient systems</li>
                    <li>Realisation of Proofs of Concept, Prototypes or MVPs</li>
                    <li>Event Sourcing / Domain Driven Design</li>
                    <li>Evolutionary Architecture - designing maintainable, testable and flexible systems</li>
                </ul>
            </div>
        </section>
    </div>
</div>