<% thisYear = published_date.getYear() + 1900 %>

<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 footer-title">
                <h2>innovensō</h2>
            </div>
            <div class="col-12 col-md-4 footer-address">
                <p>Virginie Héloire</p>
                <p>virginie@innovenso.com</p>
                <p>+33 6 69 67 71 98</p>
                <p>Jurgen Lust</p>
                <p>jurgen@innovenso.com</p>
                <p>+32 497 50 89 11</p>
                <p>Houtbriel 26</p>
                <p>9000 Gent</p>
                <p>Belgium</p>
            </div>
            <div class="col-12 col-md-4 footer-copyright">
                <p>(c) 2019 ${thisYear > 2019 ? '- ' + thisYear : ''} Innovensō</p>
                <p><a href="https://download.innovenso.io/jurgen_lust-latest.pdf">Jurgen's CV</a></p>
                <p><a href="https://download.innovenso.io/virginie_heloire-latest.pdf">Virginie's CV</a></p>
                <!--<p><a href="index.html">Terms and Conditions</a></p>-->
                <!--<p><a href="index.html">RSS feed</a></p>-->
            </div>
        </div>
    </div>
</div>
