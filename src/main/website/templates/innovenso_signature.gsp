<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="x-apple-disable-message-reformatting"><meta content="telephone=no" name="format-detection"><style>body,html{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}#signature a,#signature a:hover,#signature a:visited{color:inherit;font:inherit;text-decoration:none;pointer-events:none}*{font-family:arial,helvetica,sans-serif}html{font-family:arial,helvetica,sans-serif}body{font-family:arial,helvetica,sans-serif}table{font-family:arial,helvetica,sans-serif;border-spacing:0}td{font-family:arial,helvetica,sans-serif}th{font-family:arial,helvetica,sans-serif}p{font-family:arial,helvetica,sans-serif}span{font-family:arial,helvetica,sans-serif}div{font-family:arial,helvetica,sans-serif}a{font-family:arial,helvetica,sans-serif}img{font-family:arial,helvetica,sans-serif}.msoNormal{font-family:arial,helvetica,sans-serif}.MsoNormal{font-family:arial,helvetica,sans-serif}.externalclass{font-family:arial,helvetica,sans-serif}.ExternalClass *{line-height:100%}a[href^=tel]{color:#000;text-decoration:none}.removeStyle a{color:#000000;text-decoration:none;}</style></head>
<body>

<table style="width:600px;font-family:arial,helvetica,sans-serif;font-size:16px;color:#000000;line-height:16px;border-collapse:collapse;border-spacing:0;cell-padding:0;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;" border="0" cellspacing="0" cellpadding="0" border-collapse="collapse" width="600" valign="top">
    <tbody><tr>
        <td valign="top">
            <table id="wrapper-table " style="width:100%;font-family:arial,helvetica,sans-serif;font-size:16px;color:#000000;line-height:16px;border-collapse:collapse;border-spacing:0;cell-padding:0;" border="0" cellspacing="0" cellpadding="0" border-collapse="collapse" width="100%" valign="top">
                <tbody><tr style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:20px;font-weight:bold;" id="fullName">
                    <td style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:20px;font-weight:bold;">
                        ${content.author}
                    </td>
                </tr>
                <tr><td height="8" style="height:8px;"></td></tr>

                <tr style=" font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:16px;line-height:14px;font-weight:200;" id="jobFunction">
                    <td style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:16px;line-height:14px;font-weight:200;">
                        ${content.title}
                    </td>(
                </tr>
                <tr><td height="10" style="height:10px;"></td></tr>


                <tr>
                    <td>
                        <a href="https://innovenso.com" style="display:inline-block;cursor:pointer;color:#000000;color:#000000!important;text-decoration:none;text-decoration:none!important;">
                            <img src="https://innovenso.com/images/email-logo.png" width="200" height="49" style="width:100%;max-width:200px;height:49px;display:inline-block;border:0;font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:10px;" alt="Innovenso">
                        </a>
                    </td>
                </tr>
                <tr><td height="10" style="height:10px;line-height:1px;font-size:0px;"></td></tr>


                <tr><td height="2" style="height:2px;line-height:1px;font-size:0px;"></td></tr>
                <tr style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;" id="location">
                    <td style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;">
                        <span style="Margin:0;Margin-bottom:0;color:#000000;color:#000000!important;font-family: arial, helvetica, sans-serif;font-size:12px;font-weight:400;margin:0;margin-bottom:0;padding:0;text-align:left;text-decoration:none;text-decoration:none!important;">
                            ${content.street}<br>${content.postal} ${content.city}<br>${content.country}
                        </span>
                    </td>
                </tr>
                <tr><td height="10" style="height:10px;line-height:1px;font-size:0px;"></td></tr>



                <tr style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;line-height:16px;" id="mobilePhonenumber">
                    <td style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;line-height:16px;">
                        <span style="Margin:0;Margin-bottom:0;color:#000000;color:#000000!important;font-family: arial, helvetica, sans-serif;font-size:12px;font-weight:400;margin:0;margin-bottom:0;padding:0;text-align:left;text-decoration:none;text-decoration:none!important;">
                            M ${content.telephone}
                        </span>
                    </td>
                </tr>
                <tr><td height="10" style="height:10px;line-height:1px;font-size:0px;"></td></tr>


                <tr style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;line-height:16px;" id="emailAddress">
                    <td style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;line-height:16px;">
                        <span style="Margin:0;Margin-bottom:0;color:#000000;color:#000000!important;font-family: arial, helvetica, sans-serif;font-size:12px;font-weight:400;margin:0;margin-bottom:0;padding:0;text-align:left;line-height:16px;">
                            <a href="mailto:${content.email}" data-bindattr-370="370" style="Margin:0;color:#000000;color:#000000!important;font-family: arial, helvetica, sans-serif;font-weight:400;margin:0;padding:0;text-align:left;text-decoration:underline;">${content.email}</a>
                        </span>
                    </td>
                </tr>
                <tr><td height="10" style="height:10px;line-height:1px;font-size:0px;"></td></tr>

                <tr style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;">
                    <td style="font-family:arial,helvetica,sans-serif;color:#000000;color:#000000!important;text-decoration:none;font-size:12px;">
                        <span style="Margin:0;Margin-bottom:0;color:#000000;color:#000000!important;font-family: arial, helvetica, sans-serif;font-size:12px;font-weight:400;margin:0;margin-bottom:0;padding:0;text-align:left;text-decoration:none;text-decoration:none!important;">
                            <a href="https://innovenso.com" style="Margin:0;color:#000000;color:#000000!important;font-family: arial, helvetica, sans-serif;font-weight:400;margin:0;padding:0;text-align:left;text-decoration:underline;text-decoration:underline!important;">Innovenso</a>
                        </span>
                    </td>
                </tr>
                <tr><td height="10" style="height:10px;line-height:1px;font-size:0px;"></td></tr>





                </tbody></table>
        </td>
    </tr>
    </tbody></table>

<!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none; white-space:nowrap; font:15px courier; line-height:0;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body></html>