<div class="container-fluid header">
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-light justify-content-between">
            <a class="navbar-brand" href="${content.rootpath ?: ''}index.html">
                <img src="${content.rootpath ?: ''}images/logo-horizontal.svg" class="navbar-logo d-inline-block align-top" alt="Innovenso">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="${content.rootpath ?: ''}index.html">home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${content.rootpath ?: ''}about.html">our dna</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${content.rootpath ?: ''}services.html">our services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${content.rootpath ?: ''}contact.html">contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>