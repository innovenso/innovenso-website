<div class="container-fluid content-home">
    <div class="container">
        <div class="row">
            <section class="col-12 description background-front">
                <h1>Innovate, Simplify, Harmonise</h1>
                <p>We help organisations build their architecture practice and IT strategy, supporting innovations and digital
                transformations.</p>
                <p>Our team also develops best in class innovative IT solutions, delivering quality at speed.</p>
            </section>
        </div>
    </div>
</div>
